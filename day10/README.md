## Day 10

This solution requires human-based OCR processing. The program assumes that the points will be clustered closely together in the solution, and outputs a few samples.

The solution for Part 1 is the text in the only sample that has text, and Part 2 is the number below that sample
