package main

import "fmt"

type Rules map[int]bool

const ruleLength = 5 // Solution assumes this is odd

type State struct {
	values map[int]bool
	start  int
	end    int
}

func convertBits(bits []bool) int {
	result := 0
	for _, v := range bits {
		result *= 2
		if v {
			result += 1
		}
	}
	return result
}

func convertString(str string) []bool {
	bits := make([]bool, 0)
	for _, r := range str {
		bits = append(bits, r == '#')
	}

	return bits
}

func NewState(str string) *State {
	bits := convertString(str)

	values := make(map[int]bool)

	for id, b := range bits {
		values[id] = b
	}

	return &State{
		values: values,
		start:  0,
		end:    len(bits) - 1,
	}
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func (state *State) Iterate(rules Rules) *State {
	newValues := make(map[int]bool)
	newStart := state.end
	newEnd := state.start

	for i := state.start - ruleLength/2; i <= state.end+ruleLength/2; i++ {

		reprValue := 0
		for j := 0; j < ruleLength; j++ {
			pos := i + j - (ruleLength / 2)

			reprValue *= 2
			if state.values[pos] {
				reprValue += 1
			}
		}

		value := rules[reprValue]
		if value {
			newValues[i] = value
			newStart = min(newStart, i)
			newEnd = max(newEnd, i)
		}
	}

	return &State{
		values: newValues,
		start:  newStart,
		end:    newEnd,
	}
}

func (state *State) Print() {
	for i := state.start; i <= state.end; i++ {
		if state.values[i] {
			fmt.Print("# ")
		} else {
			fmt.Print(". ")
		}
	}
	fmt.Println()
}

func (state *State) Sum() int {
	sum := 0
	for k, v := range state.values {
		if v {
			sum += k
		}
	}

	return sum
}

func read() (string, Rules) {
	var init string
	fmt.Scanf("initial state: %s\n\n", &init)

	rules := make(map[int]bool)
	for {
		var lhs, rhs string
		if _, err := fmt.Scanf("%s => %s\n", &lhs, &rhs); err != nil {
			break
		}

		if rhs == "#" {
			rules[convertBits(convertString(lhs))] = true
		}
	}

	return init, rules
}

// This is a very hacky solution
// Iterate for some large number of epochs, hoping for convergence
// And then linearly extrapolate the answer
func main() {
	init, rules := read()

	previousSum := 0
	delta := 0

	state := NewState(init)
	for i := 0; i < 500; i++ {
		state = state.Iterate(rules)

		sum := state.Sum()
		delta = sum - previousSum
		previousSum = sum
	}

	fmt.Println(state.Sum() + (50000000000-500)*delta)
}
