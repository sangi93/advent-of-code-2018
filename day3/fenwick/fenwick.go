package fenwick

type Fenwick struct {
	arr []int
}

func (f *Fenwick) Update(x int, value int) {
	x++
	for ; x < len(f.arr); x += x & (-x) {
		f.arr[x] += value
	}
}

func (f *Fenwick) UpdateRange(start int, end int, value int) {
	f.Update(start, value)
	f.Update(end, -value)
}

func (f *Fenwick) Get(x int) int {
	x++
	sum := 0
	for ; x > 0; x -= (x & -x) {
		sum += f.arr[x]
	}
	return sum
}

func NewFenwick(size int) *Fenwick {
	return &Fenwick{arr: make([]int, size+1)}
}

type Fenwick2D struct {
	arr []*Fenwick
}

func NewFenwick2D(width int, height int) *Fenwick2D {
	fenwick := Fenwick2D{arr: make([]*Fenwick, height+1)}
	for i := 0; i < height+1; i++ {
		fenwick.arr[i] = NewFenwick(width)
	}
	return &fenwick
}

func (f *Fenwick2D) Update(x int, y int, value int) {
	y++
	for ; y < len(f.arr); y += (y & -y) {
		f.arr[y].Update(x, value)
	}
}

func (f *Fenwick2D) UpdateRange(x int, y int, right int, bottom int, value int) {
	f.Update(x, y, value)
	f.Update(right, y, -value)
	f.Update(x, bottom, -value)
	f.Update(right, bottom, value)
}

func (f *Fenwick2D) Get(x int, y int) int {
	y++
	sum := 0
	for ; y > 0; y -= (y & -y) {
		sum += f.arr[y].Get(x)
	}
	return sum
}
