package main

import "fmt"

func read() []int {
	freqs := make([]int, 0)
	for {
		var f int
		if _, err := fmt.Scanf("%d", &f); err != nil {
			break
		}
		freqs = append(freqs, f)
	}
	return freqs
}

func simulate(freqs []int) int {
	reached := make(map[int]bool)

	current := 0
	for i := 0; ; i++ {
		current += freqs[i%len(freqs)]
		if reached[current] {
			return current
		}
		reached[current] = true
	}
}

func main() {
	freqs := read()
	result := simulate(freqs)
	fmt.Println(result)
}
