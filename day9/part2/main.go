package main

/*
 This is identical to part1, except marble count is multiplied by 100 in line 105
*/

import (
	"container/list"
	"fmt"
)

type CircularLinkedList struct {
	index *list.Element
	list  *list.List
}

func NewCLL() *CircularLinkedList {
	list := list.New()
	return &CircularLinkedList{list: list}
}

func (l *CircularLinkedList) Insert(el int) {
	if l.index == nil {
		l.index = l.list.PushFront(el)
	} else {
		l.index = l.list.InsertAfter(el, l.index)
	}
}

func (l *CircularLinkedList) advanceOneForward() {
	l.index = l.index.Next()
	if l.index == nil {
		l.index = l.list.Front()
	}
}

func (l *CircularLinkedList) advanceOneBackward() {
	l.index = l.index.Prev()
	if l.index == nil {
		l.index = l.list.Back()
	}
}

func (l *CircularLinkedList) Advance(count int) {
	if count > 0 {
		for i := 0; i < count; i++ {
			l.advanceOneForward()
		}
	} else {
		for i := 0; i < -count; i++ {
			l.advanceOneBackward()
		}
	}
}

func (l *CircularLinkedList) Remove() int {
	temp := l.index
	l.advanceOneForward()
	value := l.list.Remove(temp)
	return value.(int)
}

func read() (int, int) {
	var players, marbles int
	fmt.Scanf("%d players; last marble is worth %d points", &players, &marbles)

	return players, marbles + 1
}

func solve(players int, marbles int) map[int]int {
	scores := make(map[int]int)
	board := NewCLL()
	board.Insert(0)

	for marble := 1; marble < marbles; marble++ {
		player := (marble - 1) % players
		if marble%23 == 0 {
			scores[player] += marble
			board.Advance(-7)
			scores[player] += board.Remove()
		} else {
			board.Advance(1)
			board.Insert(marble)
		}
	}

	return scores
}

func maxScore(scores map[int]int) (int, int) {
	maxPlayer := -1
	maxScore := -1
	for p, s := range scores {
		if s > maxScore {
			maxScore = s
			maxPlayer = p
		}
	}

	return maxPlayer, maxScore
}

func main() {
	players, marbles := read()
	results := solve(players, (marbles-1)*100+1)
	_, score := maxScore(results)
	fmt.Println(score)
}
