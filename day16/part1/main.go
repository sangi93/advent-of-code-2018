package main

import "fmt"

type Instruction [4]int
type Registers [4]int

type InstructionResults struct {
	before, after Registers
	instruction   Instruction
}

type fn func(i Instruction, r Registers) Registers

var interpreter = map[string]fn{
	"addr": func(i Instruction, r Registers) Registers {
		r[i[3]] = r[i[1]] + r[i[2]]
		return r
	},
	"addi": func(i Instruction, r Registers) Registers {
		r[i[3]] = r[i[1]] + i[2]
		return r
	},

	"mulr": func(i Instruction, r Registers) Registers {
		r[i[3]] = r[i[1]] * r[i[2]]
		return r
	},
	"muli": func(i Instruction, r Registers) Registers {
		r[i[3]] = r[i[1]] * i[2]
		return r
	},

	"banr": func(i Instruction, r Registers) Registers {
		r[i[3]] = r[i[1]] & r[i[2]]
		return r
	},
	"bani": func(i Instruction, r Registers) Registers {
		r[i[3]] = r[i[1]] & i[2]
		return r
	},

	"borr": func(i Instruction, r Registers) Registers {
		r[i[3]] = r[i[1]] | r[i[2]]
		return r
	},
	"bori": func(i Instruction, r Registers) Registers {
		r[i[3]] = r[i[1]] | i[2]
		return r
	},

	"setr": func(i Instruction, r Registers) Registers {
		r[i[3]] = r[i[1]]
		return r
	},
	"seti": func(i Instruction, r Registers) Registers {
		r[i[3]] = i[1]
		return r
	},

	"gtir": func(i Instruction, r Registers) Registers {
		if i[1] > r[i[2]] {
			r[i[3]] = 1
		} else {
			r[i[3]] = 0
		}
		return r
	},
	"gtri": func(i Instruction, r Registers) Registers {
		if r[i[1]] > i[2] {
			r[i[3]] = 1
		} else {
			r[i[3]] = 0
		}
		return r
	},
	"gtrr": func(i Instruction, r Registers) Registers {
		if r[i[1]] > r[i[2]] {
			r[i[3]] = 1
		} else {
			r[i[3]] = 0
		}
		return r
	},

	"eqir": func(i Instruction, r Registers) Registers {
		if i[1] == r[i[2]] {
			r[i[3]] = 1
		} else {
			r[i[3]] = 0
		}
		return r
	},
	"eqri": func(i Instruction, r Registers) Registers {
		if r[i[1]] == i[2] {
			r[i[3]] = 1
		} else {
			r[i[3]] = 0
		}
		return r
	},
	"eqrr": func(i Instruction, r Registers) Registers {
		if r[i[1]] == r[i[2]] {
			r[i[3]] = 1
		} else {
			r[i[3]] = 0
		}
		return r
	},
}

func evaluate(op string, i Instruction, r Registers) Registers {
	return interpreter[op](i, r)
}

func possibleOpcodes(ir InstructionResults) map[string]bool {
	result := make(map[string]bool)

	for op, _ := range interpreter {
		if evaluate(op, ir.instruction, ir.before) == ir.after {
			result[op] = true
		}
	}

	return result
}

func solve(observations []InstructionResults) int {
	count := 0

	for _, ir := range observations {
		if len(possibleOpcodes(ir)) >= 3 {
			count++
		}
	}

	return count
}

func read() ([]InstructionResults, []Instruction) {
	results := make([]InstructionResults, 0)
	program := make([]Instruction, 0)

	for {
		var res InstructionResults
		if _, err := fmt.Scanf("Before: [%d, %d, %d, %d]\n", &res.before[0], &res.before[1], &res.before[2], &res.before[3]); err != nil {
			break
		}
		fmt.Scanf("%d %d %d %d\n", &res.instruction[0], &res.instruction[1], &res.instruction[2], &res.instruction[3])
		fmt.Scanf("After: [%d, %d, %d, %d]\n", &res.after[0], &res.after[1], &res.after[2], &res.after[3])
		fmt.Scanf("\n")

		results = append(results, res)
	}

	fmt.Scanf("\n")

	for {
		var instruction Instruction
		if _, err := fmt.Scanf("%d %d %d %d\n", &instruction[0], &instruction[1], &instruction[2], &instruction[3]); err != nil {
			break
		}
		program = append(program, instruction)
	}

	return results, program
}

func main() {
	observations, program := read()
	fmt.Println(len(observations), len(program))

	fmt.Println(solve(observations))
}
