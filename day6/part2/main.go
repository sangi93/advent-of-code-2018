package main

import (
	"fmt"
)

type Point struct {
	x, y int
}

type Bounds struct {
	left, right, top, bottom int
}

func min(a, b int) int {
	if a < b {
		return a
	}

	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func abs(a int) int {
	if a > 0 {
		return a
	} else {
		return -a
	}
}

func NewBounds(p Point) Bounds {
	return Bounds{left: p.x, right: p.x, top: p.y, bottom: p.y}
}

func (bounds *Bounds) Extend(point Point) {
	bounds.left = min(bounds.left, point.x)
	bounds.right = max(bounds.right, point.x)
	bounds.top = min(bounds.top, point.y)
	bounds.bottom = max(bounds.bottom, point.y)
}

func (bounds *Bounds) Width() int {
	return bounds.right - bounds.left + 1
}

func (bounds *Bounds) Height() int {
	return bounds.bottom - bounds.top + 1
}

func (p Point) OnBounds(bounds Bounds) bool {
	return (p.x == bounds.left || p.x == bounds.right) &&
		(p.y == bounds.top || p.y == bounds.bottom)
}

func (p Point) InBounds(bounds Bounds) bool {
	return p.x >= bounds.left && p.x <= bounds.right &&
		p.y >= bounds.top && p.y <= bounds.bottom
}

func (p Point) Normalised(bounds Bounds) Point {
	return Point{
		x: p.x - bounds.left,
		y: p.y - bounds.top,
	}
}

func (p Point) Distance(dest Point) int {
	return abs(p.x-dest.x) + abs(p.y-dest.y)
}

func read() []Point {
	result := make([]Point, 0)
	for {
		var point Point

		_, err := fmt.Scanf("%d, %d", &point.x, &point.y)
		if err != nil {
			break
		}

		result = append(result, point)
	}

	return result
}

func pointBounds(points []Point) Bounds {
	bounds := NewBounds(points[0])

	for _, p := range points[1:] {
		bounds.Extend(p)
	}

	return bounds
}

func sumOfDistances(point Point, origins []Point) int {
	sum := 0
	for _, p := range origins {
		sum += point.Distance(p)
	}

	return sum
}

func closeAreas(bounds Bounds, points []Point, threshold int) int {
	count := 0
	for y := 0; y < bounds.Height(); y++ {
		for x := 0; x < bounds.Width(); x++ {
			p := Point{
				x: x + bounds.left,
				y: y + bounds.top,
			}

			distances := sumOfDistances(p, points)
			if distances < threshold {
				count++
			}
		}
	}

	return count
}

func main() {
	allPoints := read()

	bounds := pointBounds(allPoints)

	result := closeAreas(bounds, allPoints, 10000)

	fmt.Println(result)
}
