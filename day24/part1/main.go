package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

type Side int

const (
	ImmuneSystem Side = iota
	Infection Side = iota
)

type AttackType string

type Group struct {
	units, hitpoints int

	weaknesses, immunities map[AttackType]bool

	damage int
	attackType AttackType

	initative int

	side Side

	available bool // Used for target selection
}

func (g Group) EffectivePower() int {
	return g.units * g.damage
}

func read() []*Group {
	result := make([]*Group, 0)

	lineRegex := regexp.MustCompile("(\\d+) units each with (\\d+) hit points(?: )?(\\(.+\\))? with an attack that does (\\d+) (\\w+) damage at initiative (\\d+)")
	modifiersRegex := regexp.MustCompile("\\((?:(\\w+) to ([a-z, ]+))(?:; (\\w+) to ([a-z, ]+))?\\)")

	currentSide := ImmuneSystem

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := scanner.Text()
		if line == "Immune System:" {
			currentSide = ImmuneSystem
			continue
		} else if line == "Infection:" {
			currentSide = Infection
			continue
		} else if line == "" {
			continue
		}
		groups := lineRegex.FindStringSubmatch(line)

		var group Group
		group.units, _ = strconv.Atoi(groups[1])
		group.hitpoints, _ = strconv.Atoi(groups[2])
		group.damage, _ = strconv.Atoi(groups[4])
		group.attackType = AttackType(groups[5])
		group.initative, _ = strconv.Atoi(groups[6])
		group.side = currentSide

		if groups[3] != "" {
			modifiers := modifiersRegex.FindStringSubmatch(groups[3])

			for i := 1; i < len(modifiers); i += 2 {
				typesStrings := strings.Split(modifiers[i+1], ", ")
				types := make(map[AttackType]bool)
				for v := range typesStrings {
					types[AttackType(v)] = true
				}
				if modifiers[i] == "weak" {
					group.weaknesses = types
				} else if modifiers[i] == "immune" {
					group.immunities = types
				}
			}
		}

		result = append(result, &group)
	}

	return result
}

func getDamage(attacker Group, defender Group) int {
	if defender.immunities[attacker.attackType] {
		return 0
	}
	if defender.weaknesses[attacker.attackType] {
		return 2 * attacker.EffectivePower()
	}
	return attacker.EffectivePower()
}

func selectTarget(g *Group, targets []*Group) *Group {
	var result *Group
	maxDamage := 0

	for _, t := range targets {
		if t.side == g.side || !t.available || t.units == 0 {
			continue
		}
		damage := getDamage(*g, *t)
		if damage == 0 {
			continue
		}
		if damage > maxDamage ||
			damage == maxDamage && t.EffectivePower() > result.EffectivePower() ||
			damage == maxDamage && t.EffectivePower() == result.EffectivePower() && t.initative > result.initative {
				maxDamage = damage
				result = t
		}
	}

	return result
}

func attack(attacker *Group, defender *Group) {
	damage := getDamage(*attacker, *defender)
	targetsKilled := damage / defender.hitpoints
	defender.units -= targetsKilled
}

func step(groups []*Group) {
	sort.Slice(groups, func(i, j int) bool {
		if groups[i].EffectivePower() == groups[j].EffectivePower() {
			return groups[i].initative > groups[j].initative
		}
		return groups[i].EffectivePower() > groups[j].EffectivePower()
	})

	for _, g := range groups {
		g.available = true
	}

	targets := make(map[*Group]*Group)
	for _, attacker := range groups {
		fmt.Println(attacker.EffectivePower())
		if attacker.units == 0 {
			continue
		}
		defender := selectTarget(attacker, groups)
		targets[attacker] = defender
		if defender != nil {
			defender.available = false
		}
	}

	for _, attacker := range groups {
		defender := targets[attacker]
		fmt.Println(attacker, defender)
		if attacker.units == 0 {
			continue
		}
		if defender != nil {
			attack(attacker, defender)
		}
	}
}


func main() {
	groups := read()
	for _, g := range groups {
		fmt.Println(g)
	}
	fmt.Println()
	step(groups)
	for _, g := range groups {
		fmt.Println(g)
	}
}
