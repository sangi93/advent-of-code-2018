package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"sort"
	"strconv"
)

type Action int

const (
	Arrives     Action = 0
	WakesUp     Action = 1
	FallsAsleep Action = 2
)

type Event struct {
	time   string
	minute int
	action Action
	guard  int
}

func readEvents() []Event {
	eventRegex := regexp.MustCompile(`\[(.+:(\d{2}))\] (.+)`)
	actionGuardRegex := regexp.MustCompile(`Guard #(\d+) begins shift`)
	events := make([]Event, 0)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		var event Event

		line := scanner.Text()
		lineMatches := eventRegex.FindStringSubmatch(line)

		event.time = lineMatches[1]
		event.minute, _ = strconv.Atoi(lineMatches[2])

		if lineMatches[3] == "wakes up" {
			event.action = WakesUp
		} else if lineMatches[3] == "falls asleep" {
			event.action = FallsAsleep
		} else {
			guardMatches := actionGuardRegex.FindStringSubmatch(lineMatches[3])
			event.action = Arrives
			event.guard, _ = strconv.Atoi(guardMatches[1])
		}

		events = append(events, event)
	}
	return events
}

func preprocessEvents(events []Event) []Event {
	sort.Slice(events, func(i, j int) bool {
		return events[i].time < events[j].time
	})

	currentGuard := 0
	for id, _ := range events {
		event := &events[id]
		if event.action == Arrives {
			currentGuard = event.guard
		} else {
			event.guard = currentGuard
		}
	}

	return events
}

// This assumes data is in correct order and format
func mostCommonGuard(events []Event) (int, int) {
	minutesAsleep := make(map[int]int)
	fallenAsleepAt := 0
	for _, event := range events {
		if event.action == FallsAsleep {
			fallenAsleepAt = event.minute
		} else if event.action == WakesUp {
			minutesAsleep[event.guard] += event.minute - fallenAsleepAt
		}
	}

	mostCommon := -1
	mostTimeAsleep := -1
	for guard, time := range minutesAsleep {
		if time > mostTimeAsleep {
			mostCommon = guard
			mostTimeAsleep = time
		}
	}

	return mostCommon, mostTimeAsleep
}

func mostCommonMinute(events []Event, guard int) (int, int) {
	asleepChanges := make([]int, 60)
	for _, event := range events {
		if event.guard == guard {
			if event.action == FallsAsleep {
				asleepChanges[event.minute] += 1
			} else if event.action == WakesUp {
				asleepChanges[event.minute] -= 1
			}
		}
	}

	bestMinute := -1
	bestCount := -1
	currentCount := 0
	for minute, value := range asleepChanges {
		currentCount += value
		if currentCount > bestCount {
			bestMinute = minute
			bestCount = currentCount
		}
	}

	return bestMinute, bestCount
}

func allGuards(events []Event) []int {
	guardsMap := map[int]bool{}

	for _, event := range events {
		guardsMap[event.guard] = true
	}

	guards := []int{}
	for guard := range guardsMap {
		guards = append(guards, guard)
	}

	return guards
}

func mostFrequentGuardMinute(events []Event) (int, int) {
	guards := allGuards(events)

	bestGuard := -1
	bestCount := -1
	bestMinute := -1

	for _, guard := range guards {
		minute, count := mostCommonMinute(events, guard)
		if count > bestCount {
			bestCount = count
			bestMinute = minute
			bestGuard = guard
		}
	}

	return bestGuard, bestMinute
}

func main() {
	events := preprocessEvents(readEvents())

	guard, minute := mostFrequentGuardMinute(events)

	fmt.Println(guard * minute)
}
