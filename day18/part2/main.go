package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type Map [][]rune

func NewMap(width, height int) Map {
	result := make([][]rune, height)
	for y := 0; y < height; y++ {
		result[y] = make([]rune, width)
	}

	return Map(result)
}

func (m *Map) Print() {
	for y := 0; y < len(*m); y++ {
		for x := 0; x < len((*m)[y]); x++ {
			fmt.Print(string(m.Get(x, y)))
		}
		fmt.Println()
	}
}

func (m *Map) ToString() string {
	res := strings.Builder{}
	for y := 0; y < len(*m); y++ {
		for x := 0; x < len((*m)[y]); x++ {
			res.WriteRune(m.Get(x, y))
		}
		res.WriteRune('\n')
	}
	return res.String()
}

func (m *Map) Set(x, y int, v rune) {
	(*m)[y][x] = v
}

func (m *Map) Get(x, y int) rune {
	return (*m)[y][x]
}

func (m *Map) AdjacentCounts(x, y int) map[rune]int {
	result := make(map[rune]int)

	for py := y - 1; py < y+2; py++ {
		for px := x - 1; px < x+2; px++ {
			if py < 0 || py >= len(*m) ||
				px < 0 || px >= len((*m)[py]) ||
				(px == x && py == y) {
				continue
			}
			result[m.Get(px, py)]++
		}
	}

	return result
}

func (m *Map) Counts() map[rune]int {
	result := make(map[rune]int)
	for y := 0; y < len(*m); y++ {
		for x := 0; x < len((*m)[y]); x++ {
			result[m.Get(x, y)]++
		}
	}

	return result
}

func read() Map {
	result := make([][]rune, 0)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		result = append(result, []rune(scanner.Text()))
	}

	return Map(result)
}

func simulate(m Map) Map {
	result := NewMap(len(m[0]), len(m))

	for y := 0; y < len(m); y++ {
		for x := 0; x < len(m[y]); x++ {
			v := m.Get(x, y)
			counts := m.AdjacentCounts(x, y)
			//fmt.Println(x, y, counts)
			if v == '.' {
				if counts['|'] >= 3 {
					result.Set(x, y, '|')
				} else {
					result.Set(x, y, '.')
				}
			} else if v == '|' {
				if counts['#'] >= 3 {
					result.Set(x, y, '#')
				} else {
					result.Set(x, y, '|')
				}
			} else if v == '#' {
				if counts['#'] == 0 || counts['|'] == 0 {
					result.Set(x, y, '.')
				} else {
					result.Set(x, y, '#')
				}
			}
		}
	}

	return result
}

func main() {
	m := read()

	seen := make(map[string]int)

	count := 1000000000

	for i := 0; i < count; i++ {
		m = simulate(m)
		str := m.ToString()
		prevIter := seen[str]
		if prevIter > 0 {
			period := i - prevIter
			left := count - i
			repeats := left / period
			i += repeats * period
		}
		seen[str] = i
	}

	counts := m.Counts()
	fmt.Println(counts['|'] * counts['#'])
}
