package main

import "fmt"

type Nanobot struct {
	x, y, z int
	radius  int
}

func abs(a int) int {
	if a > 0 {
		return a
	}
	return -a
}

func (b1 Nanobot) Distance(b2 Nanobot) int {
	return abs(b1.x-b2.x) + abs(b1.y-b2.y) + abs(b1.z-b2.z)
}

func (b Nanobot) DistanceToOrigin() int {
	return b.Distance(Nanobot{0, 0, 0, 0})
}

func read() []Nanobot {
	result := make([]Nanobot, 0)

	for {
		var bot Nanobot
		if _, err := fmt.Scanf("pos=<%d,%d,%d>, r=%d\n", &bot.x, &bot.y, &bot.z, &bot.radius); err != nil {
			break
		}

		result = append(result, bot)
	}

	return result
}

func inRange(bots []Nanobot, target Nanobot, threshold int) int {
	result := 0

	for _, b := range bots {
		if b.Distance(target)-b.radius < threshold {
			result++
		}
	}

	return result
}

type Bounds struct {
	left, right, top, bottom, back, forward int
}

func NewBounds(n Nanobot) Bounds {
	return Bounds{
		n.x,
		n.x,
		n.y,
		n.y,
		n.z,
		n.z,
	}
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func (b Bounds) Extend(n Nanobot) Bounds {
	return Bounds{
		min(b.left, n.x),
		max(b.right, n.x),
		min(b.top, n.y),
		max(b.bottom, n.y),
		min(b.back, n.z),
		max(b.forward, n.z),
	}
}

func findBounds(bots []Nanobot) Bounds {
	result := NewBounds(bots[0])
	for _, b := range bots {
		result = result.Extend(b)
	}

	return result
}

func solveStep(bots []Nanobot, bounds Bounds, step int) Nanobot {
	bestBot := Nanobot{}
	bestCount := -1

	for x := bounds.left; x <= bounds.right; x += step {
		for y := bounds.top; y <= bounds.bottom; y += step {
			for z := bounds.back; z <= bounds.forward; z += step {
				bot := Nanobot{x, y, z, step}
				count := inRange(bots, bot, step)

				if count > bestCount {
					bestBot = bot
					bestCount = count
				}
				if count == bestCount && bot.DistanceToOrigin() < bestBot.DistanceToOrigin() {
					bestBot = bot
				}
			}
		}
	}

	if step == 1 {
		//Hopefully found the best answer
		return bestBot
	} else {
		newBounds := Bounds{
			bestBot.x - step,
			bestBot.x + step,
			bestBot.y - step,
			bestBot.y + step,
			bestBot.z - step,
			bestBot.z + step,
		}
		return solveStep(bots, newBounds, step/2)
	}
}

func solve(bots []Nanobot) int {
	bounds := findBounds(bots)

	bestBot := solveStep(bots, bounds, bounds.right-bounds.left)

	return bestBot.DistanceToOrigin()
}

func main() {
	bots := read()
	result := solve(bots)
	fmt.Println(result)
}
