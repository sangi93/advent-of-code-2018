package main

import "fmt"

type Point struct {
	x, y int
}

type Map [][]int

func NewMap(target Point) Map {
	result := make(Map, target.y+1)
	for y := 0; y <= target.y; y++ {
		result[y] = make([]int, target.x+1)
	}

	return result
}

var symbols = []string{".", "=", "|"}

func (m Map) Print() {
	for y := 0; y < len(m); y++ {
		for x := 0; x < len(m[y]); x++ {
			fmt.Print(symbols[m[y][x]])
		}
		fmt.Println()
	}
}

func (m Map) Sum() int {
	result := 0
	for y := 0; y < len(m); y++ {
		for x := 0; x < len(m[y]); x++ {
			result += m[y][x]
		}
	}

	return result
}

const modulo = 20183

func generateMap(depth int, target Point) Map {
	m := NewMap(target)

	for y := 0; y <= target.y; y++ {
		for x := 0; x <= target.x; x++ {
			if y == 0 {
				m[y][x] = (x * 16807) % modulo
			} else if x == 0 {
				m[y][x] = (y * 48271) % modulo
			} else if x == target.x && y == target.y {
				m[y][x] = 0
			} else {
				m[y][x] = (m[y-1][x] * m[y][x-1]) % modulo
			}
			m[y][x] = (m[y][x] + depth) % modulo
		}
	}

	for y := 0; y <= target.y; y++ {
		for x := 0; x <= target.x; x++ {
			m[y][x] %= 3
		}
	}

	return m
}

func read() (int, Point) {
	var depth int
	var target Point
	fmt.Scanf("depth: %d", &depth)
	fmt.Scanf("target: %d,%d", &target.x, &target.y)

	return depth, target
}

func main() {
	depth, target := read()
	m := generateMap(depth, target)
	fmt.Println(m.Sum())
}
