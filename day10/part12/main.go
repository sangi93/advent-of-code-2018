package main

import (
	"errors"
	"fmt"
)

type Point struct {
	x, y, dx, dy int
}

func (p *Point) Iterate() {
	p.x += p.dx
	p.y += p.dy
}

func (p Point) Normalised(bounds Bounds) (Point, error) {
	if p.x < bounds.left || p.y < bounds.top ||
		p.x > bounds.right || p.y > bounds.bottom {
		return Point{}, errors.New("point out of bounds")
	}

	result := Point{
		x:  p.x - bounds.left,
		y:  p.y - bounds.top,
		dx: p.dx,
		dy: p.dy,
	}
	return result, nil
}

type Points []Point

type Bounds struct {
	top, bottom, left, right int
}

func NewBounds(p Point) Bounds {
	return Bounds{top: p.y, bottom: p.y, left: p.x, right: p.x}
}

func ordered(a, b int) (int, int) {
	if a < b {
		return a, b
	}
	return b, a
}

func (b *Bounds) Extend(p Point) {
	b.left, _ = ordered(b.left, p.x)
	_, b.right = ordered(b.right, p.x)
	b.top, _ = ordered(b.top, p.y)
	_, b.bottom = ordered(b.bottom, p.y)
}

func (b Bounds) Width() int {
	return b.right - b.left + 1
}

func (b Bounds) Height() int {
	return b.bottom - b.top + 1
}

func read() Points {
	result := make([]Point, 0)
	for {
		var p Point
		if _, err := fmt.Scanf("position=<%d, %d> velocity=<%d, %d>\n", &p.x, &p.y, &p.dx, &p.dy); err != nil {
			fmt.Println(err)
			break
		}

		result = append(result, p)
	}

	return result
}

func (points *Points) Bounds() Bounds {
	b := NewBounds(([]Point(*points))[0])
	for _, p := range *points {
		b.Extend(p)
	}
	return b
}

func (points *Points) Iterate() {
	for id, p := range *points {
		p.Iterate()

		([]Point(*points))[id] = p
	}
}

func (points *Points) Print(bounds Bounds) {
	// This is highly inefficient, would be much better to sort the points first

	output := make([][]bool, bounds.Height())
	for i := 0; i < bounds.Height(); i++ {
		output[i] = make([]bool, bounds.Width())
	}

	for _, p := range *points {
		if normal, err := p.Normalised(bounds); err == nil {
			output[normal.y][normal.x] = true
		}
	}

	for y := 0; y < bounds.Height(); y++ {
		for x := 0; x < bounds.Width(); x++ {
			if output[y][x] {
				fmt.Print("#")
			} else {
				fmt.Print(".")
			}
		}
		fmt.Println()
	}
}

func main() {
	points := read()

	// This program requires user attention
	viableBounds := 80

	t := 0
	seenClose := false
	for {
		bounds := points.Bounds()
		if bounds.Width() <= viableBounds && bounds.Height() <= viableBounds {
			points.Print(bounds)
			fmt.Println(t)

			seenClose = true
		} else if seenClose {
			// Break execution, we have seen close solutions already
			break
		}
		points.Iterate()
		t++
	}
}
