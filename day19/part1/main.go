package main

import (
	"fmt"
)

type Parameters [3]int
type Instruction struct {
	opCode string
	params Parameters
}
type Registers [6]int
type Program struct {
	instructions []Instruction
	ip           int
	registers    Registers
	step         int
}

type fn func(i Parameters, r Registers) Registers

var interpreter = map[string]fn{
	"addr": func(i Parameters, r Registers) Registers {
		r[i[2]] = r[i[0]] + r[i[1]]
		return r
	},
	"addi": func(i Parameters, r Registers) Registers {
		r[i[2]] = r[i[0]] + i[1]
		return r
	},

	"mulr": func(i Parameters, r Registers) Registers {
		r[i[2]] = r[i[0]] * r[i[1]]
		return r
	},
	"muli": func(i Parameters, r Registers) Registers {
		r[i[2]] = r[i[0]] * i[1]
		return r
	},

	"banr": func(i Parameters, r Registers) Registers {
		r[i[2]] = r[i[0]] & r[i[1]]
		return r
	},
	"bani": func(i Parameters, r Registers) Registers {
		r[i[2]] = r[i[0]] & i[1]
		return r
	},

	"borr": func(i Parameters, r Registers) Registers {
		r[i[2]] = r[i[0]] | r[i[1]]
		return r
	},
	"bori": func(i Parameters, r Registers) Registers {
		r[i[2]] = r[i[0]] | i[1]
		return r
	},

	"setr": func(i Parameters, r Registers) Registers {
		r[i[2]] = r[i[0]]
		return r
	},
	"seti": func(i Parameters, r Registers) Registers {
		r[i[2]] = i[0]
		return r
	},

	"gtir": func(i Parameters, r Registers) Registers {
		if i[0] > r[i[1]] {
			r[i[2]] = 1
		} else {
			r[i[2]] = 0
		}
		return r
	},
	"gtri": func(i Parameters, r Registers) Registers {
		if r[i[0]] > i[1] {
			r[i[2]] = 1
		} else {
			r[i[2]] = 0
		}
		return r
	},
	"gtrr": func(i Parameters, r Registers) Registers {
		if r[i[0]] > r[i[1]] {
			r[i[2]] = 1
		} else {
			r[i[2]] = 0
		}
		return r
	},

	"eqir": func(i Parameters, r Registers) Registers {
		if i[0] == r[i[1]] {
			r[i[2]] = 1
		} else {
			r[i[2]] = 0
		}
		return r
	},
	"eqri": func(i Parameters, r Registers) Registers {
		if r[i[0]] == i[1] {
			r[i[2]] = 1
		} else {
			r[i[2]] = 0
		}
		return r
	},
	"eqrr": func(i Parameters, r Registers) Registers {
		if r[i[0]] == r[i[1]] {
			r[i[2]] = 1
		} else {
			r[i[2]] = 0
		}
		return r
	},
}

func read() Program {
	var program Program
	fmt.Scanf("#ip %d\n", &program.ip)
	for {
		var instruction Instruction
		if _, err := fmt.Scanf("%s %d %d %d", &instruction.opCode, &instruction.params[0], &instruction.params[1], &instruction.params[2]); err != nil {
			break
		}
		program.instructions = append(program.instructions, instruction)
	}
	program.registers[0] = 0

	return program
}

func emulate(program Program) Program {
	for program.step = 0; program.registers[program.ip] < len(program.instructions); program.step++ {
		instruction := program.instructions[program.registers[program.ip]]
		program.registers = interpreter[instruction.opCode](instruction.params, program.registers)
		program.registers[program.ip]++
	}
	program.registers[program.ip]--
	return program
}

func main() {
	program := read()
	fmt.Println(emulate(program).registers[0])
}
