package main

import "fmt"

func read() []string {
	results := make([]string, 0)
	for {
		var res string
		if _, err := fmt.Scanf("%s", &res); err != nil {
			break
		}

		results = append(results, res)
	}

	return results
}

func distance(a, b string) (int, string) {
	runesA := []rune(a)
	runesB := []rune(b)

	dist := 0
	common := make([]rune, 0)

	for id, value := range runesA {
		if value != runesB[id] {
			dist++
		} else {
			common = append(common, value)
		}
	}

	return dist, string(common)
}

func solve(values []string) string {
	for i, a := range values {
		for j, b := range values {
			if j == i {
				break
			}

			dist, common := distance(a, b)
			if dist == 1 {
				return common
			}
		}
	}
	return ""
}

func main() {
	values := read()

	fmt.Println(solve(values))
}
