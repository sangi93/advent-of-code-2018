package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"time"
	//"time"
)

type Queue []interface{}

func (q *Queue) Push(d interface{}) {
	*q = append(*q, d)
}

func (q *Queue) Size() int {
	return len(*q)
}

func (q *Queue) Empty() bool {
	return q.Size() == 0
}

func (q *Queue) Back() interface{} {
	return []interface{}(*q)[0]
}

func (q *Queue) Pop() interface{} {
	back := q.Back()
	*q = []interface{}(*q)[1:]
	return back
}

type Map struct {
	field [][]bool // False for walkable, true for impassable
}

type PlayerType int

const (
	Elf    PlayerType = 0
	Goblin PlayerType = 1
)

func (pt PlayerType) toString() string {
	if pt == Elf {
		return "E"
	} else {
		return "G"
	}
}

type Player struct {
	playerType PlayerType
	x, y       int
	health     int
	attack     int
	alive      bool
}

func playerMask(fields *Map, players *[]Player) [][]Player {
	playerFields := make([][]Player, len(fields.field))
	for i := 0; i < len(fields.field); i++ {
		playerFields[i] = make([]Player, len(fields.field[i]))
	}

	for _, p := range *players {
		if p.alive {
			playerFields[p.y][p.x] = p
		}
	}

	return playerFields
}

func sortPlayers(players *[]Player) {
	sort.Slice(*players, func(i, j int) bool {
		if (*players)[i].y == (*players)[j].y {
			return (*players)[i].x < (*players)[j].x
		} else {
			return (*players)[i].y < (*players)[j].y
		}
	})
}

type DistanceDirection struct {
	dx, dy   int
	distance int
}

type QueueElement struct {
	x, y     int
	distance int
}

func abs(a int) int {
	if a > 0 {
		return a
	}
	return -a
}

const INF = 10000000

func simulateSinglePlayer(fields *Map, player *Player, otherPlayers *[]Player) bool {
	playerFields := playerMask(fields, otherPlayers)
	backTrackDirection := make([][]DistanceDirection, len(fields.field))
	for y := 0; y < len(fields.field); y++ {
		backTrackDirection[y] = make([]DistanceDirection, len(fields.field[y]))
		for x := 0; x < len(backTrackDirection[y]); x++ {
			backTrackDirection[y][x] = DistanceDirection{distance: INF}
		}
	}

	//Step 1: find shortest path
	q := make(Queue, 0)
	q.Push(QueueElement{
		x:        player.x,
		y:        player.y,
		distance: 0,
	})
	backTrackDirection[player.y][player.x].distance = 0

	closestX := INF
	closestY := INF
	closestDistance := INF

	for !q.Empty() {
		element := q.Pop().(QueueElement)

		if element.distance >= closestDistance {
			break
		}

		for dy := -1; dy <= 1; dy++ {
			for dx := -1; dx <= 1; dx++ {
				if dx != dy && -dx != dy { // Only allows cardinal directions
					dp := QueueElement{
						x:        element.x + dx,
						y:        element.y + dy,
						distance: element.distance + 1,
					}

					if backTrackDirection[dp.y][dp.x].distance == element.distance-1 &&
						backTrackDirection[element.y][element.x].dx == 0 &&
						backTrackDirection[element.y][element.x].dy == 0 {
						backTrackDirection[element.y][element.x].dx = dx
						backTrackDirection[element.y][element.x].dy = dy
					}
					if backTrackDirection[dp.y][dp.x].distance > element.distance+1 &&
						!fields.field[dp.y][dp.x] {

						possiblePlayer := playerFields[dp.y][dp.x]
						// Real player
						if possiblePlayer.alive {
							if possiblePlayer.playerType != player.playerType {
								if element.y < closestY ||
									closestY == element.y && element.x < closestX {
									closestX = element.x
									closestY = element.y
									closestDistance = dp.distance
								}
							}
						} else {
							backTrackDirection[dp.y][dp.x].distance = dp.distance
							q.Push(dp)
						}
					}
				}
			}
		}
	}

	/*
		for y := 0; y < len(backTrackDirection); y++ {
			for x := 0; x < len(backTrackDirection[y]); x++ {
				if backTrackDirection[y][x].distance < INF {
					if backTrackDirection[y][x].dx == -1 {
						fmt.Print("<")
					} else if backTrackDirection[y][x].dx == 1 {
						fmt.Print(">")
					} else if backTrackDirection[y][x].dy == -1 {
						fmt.Print("^")
					} else if backTrackDirection[y][x].dy == 1 {
						fmt.Print("v")
					} else {
						fmt.Print("x")
					}
				} else {
					if playerFields[y][x].attack > 0 {
						fmt.Print("*")
					} else {
						fmt.Print(".")
					}
				}
			}
			fmt.Println()
		}
	*/

	if closestDistance == INF {
		return false
	}

	for {
		dx := backTrackDirection[closestY][closestX].dx
		dy := backTrackDirection[closestY][closestX].dy

		newX := closestX + dx
		newY := closestY + dy

		if newX == player.x && newY == player.y {
			break
		}

		closestX = newX
		closestY = newY
	}

	player.x = closestX
	player.y = closestY

	lowestEnemy := Player{x: INF, y: INF, health: INF}
	lowestEnemyId := -1

	for id, enemy := range *otherPlayers {
		if enemy.playerType != player.playerType &&
			enemy.alive &&
			abs(enemy.x-player.x)+abs(enemy.y-player.y) == 1 &&
			(enemy.health < lowestEnemy.health ||
				enemy.health == lowestEnemy.health &&
					enemy.y < lowestEnemy.y ||
				enemy.y == lowestEnemy.y && enemy.x < lowestEnemy.x) {

			lowestEnemy = enemy
			lowestEnemyId = id
		}
	}

	if lowestEnemyId != -1 {

		lowestEnemy.health -= player.attack
		if lowestEnemy.health <= 0 {
			lowestEnemy.alive = false
		}
		(*otherPlayers)[lowestEnemyId] = lowestEnemy

		if lowestEnemy.health <= 0 {
			return true
		}
	}

	return false
}

func aliveCounts(players *[]Player) (int, int) {
	elvesRemaining := 0
	goblinsRemaining := 0
	for _, player := range *players {
		if player.alive {
			if player.playerType == Elf {
				elvesRemaining++
			} else {
				goblinsRemaining++
			}
		}
	}
	return elvesRemaining, goblinsRemaining
}

func deadCounts(players *[]Player) (int, int) {
	elvesDead := 0
	goblinsDead := 0
	for _, player := range *players {
		if !player.alive {
			if player.playerType == Elf {
				elvesDead++
			} else {
				goblinsDead++
			}
		}
	}
	return elvesDead, goblinsDead
}

func totalHealth(players *[]Player) int {
	sum := 0
	for _, player := range *players {
		if player.alive {
			sum += player.health
		}
	}

	return sum
}

// true if completed, false if not
func simulateRound(fields *Map, players *[]Player) bool {
	sortPlayers(players)

	if elves, goblins := aliveCounts(players); elves == 0 || goblins == 0 {
		return false
	}

	for id, p := range *players {
		if !p.alive {
			continue
		}
		if elves, goblins := aliveCounts(players); elves == 0 || goblins == 0 {
			return false
		}
		if simulateSinglePlayer(fields, &p, players) && p.playerType == Goblin {
			return false // Goblin killed an Elf
		}

		(*players)[id] = p
	}

	return true
}

func print(fields *Map, players *[]Player) {

	printMap := make([][]string, len(fields.field))
	for i := 0; i < len(fields.field); i++ {
		printMap[i] = make([]string, len(fields.field[i]))
		for x, v := range fields.field[i] {
			if v {
				printMap[i][x] = "#"
			} else {
				printMap[i][x] = "."
			}
		}
	}

	playerYMap := make(map[int][]Player)

	for _, p := range *players {
		if !p.alive {
			continue
		}

		printMap[p.y][p.x] = p.playerType.toString()

		if playerYMap[p.y] == nil {
			playerYMap[p.y] = make([]Player, 0)
		}

		playerYMap[p.y] = append(playerYMap[p.y], p)
	}

	for y := 0; y < len(printMap); y++ {
		for x := 0; x < len(printMap[y]); x++ {
			fmt.Print(printMap[y][x])
		}

		fmt.Print("\t")
		if playerYMap[y] != nil {
			for id, p := range playerYMap[y] {
				if id != 0 {
					fmt.Print(", ")
				}
				fmt.Printf("%s(%d)", p.playerType.toString(), p.health)
			}
		}

		fmt.Println()
	}

}

func readPlayers(line string, y int) ([]bool, []Player) {
	fieldLine := make([]bool, len(line))
	players := make([]Player, 0)

	for id, value := range line {
		if value == '#' {
			fieldLine[id] = true
		} else if value == '.' {
			fieldLine[id] = false
		} else {
			fieldLine[id] = false
			var playerType PlayerType
			if value == 'G' {
				playerType = Goblin
			} else if value == 'E' {
				playerType = Elf
			}
			players = append(players, Player{
				playerType: playerType,
				x:          id,
				y:          y,
				health:     200,
				attack:     3,
				alive:      true,
			})
		}
	}

	return fieldLine, players
}

func read() (*Map, *[]Player) {
	fields := make([][]bool, 0)
	players := make([]Player, 0)

	scanner := bufio.NewScanner(os.Stdin)
	for i := 0; scanner.Scan(); i++ {
		line, newPlayers := readPlayers(scanner.Text(), i)
		fields = append(fields, line)
		players = append(players, newPlayers...)
	}

	return &Map{fields}, &players
}

const showAnimation = false

func runSimulation(fields *Map, players []Player, elfAttack int) int {

	//fmt.Println("Trying", elfAttack)

	for id, p := range players {
		if p.playerType == Elf {
			p.attack = elfAttack
			players[id] = p
		}
	}

	i := 0
	for ; simulateRound(fields, &players); i++ {
		if showAnimation {
			fmt.Print("\033[H\033[2J")
			fmt.Println("Round:", i+1)
			sortPlayers(&players)
			print(fields, &players)
			time.Sleep(50 * time.Millisecond)
		}
	}

	if elves, _ := deadCounts(&players); elves > 0 {
		return -1
	}
	fmt.Println(elfAttack)
	print(fields, &players)
	health := totalHealth(&players)
	return i * health
}

func main() {
	fields, players := read()

	for i := 3; ; i++ {
		playersCopy := make([]Player, len(*players))
		copy(playersCopy, *players)
		result := runSimulation(fields, playersCopy, i)
		if result != -1 {
			fmt.Println(result)
			return
		}
	}
}
