package main

import (
	"fmt"
	"regexp"
	"strings"
)

type Parameters [3]int
type Instruction struct {
	opCode string
	params Parameters
}
type Registers [6]int
type Program struct {
	instructions []Instruction
	ip           int
	registers    Registers
	step         int
}

type fn func(i Parameters) string

var interpreter = map[string]fn{
	"addr": func(i Parameters) string {
		return fmt.Sprintf("%s = %s + %s", variables[i[2]], variables[i[0]], variables[i[1]])
	},
	"addi": func(i Parameters) string {
		return fmt.Sprintf("%s = %s + %d", variables[i[2]], variables[i[0]], i[1])
	},

	"mulr": func(i Parameters) string {
		return fmt.Sprintf("%s = %s * %s", variables[i[2]], variables[i[0]], variables[i[1]])
	},
	"muli": func(i Parameters) string {
		return fmt.Sprintf("%s = %s * %d", variables[i[2]], variables[i[0]], i[1])
	},

	"banr": func(i Parameters) string {
		return fmt.Sprintf("%s = %s & %s", variables[i[2]], variables[i[0]], variables[i[1]])
	},
	"bani": func(i Parameters) string {
		return fmt.Sprintf("%s = %s & %d", variables[i[2]], variables[i[0]], i[1])
	},

	"borr": func(i Parameters) string {
		return fmt.Sprintf("%s = %s | %s", variables[i[2]], variables[i[0]], variables[i[1]])
	},
	"bori": func(i Parameters) string {
		return fmt.Sprintf("%s = %s | %d", variables[i[2]], variables[i[0]], i[1])
	},

	"setr": func(i Parameters) string {
		return fmt.Sprintf("%s = %s", variables[i[2]], variables[i[0]])
	},
	"seti": func(i Parameters) string {
		return fmt.Sprintf("%s = %d", variables[i[2]], i[0])
	},

	"gtir": func(i Parameters) string {
		return fmt.Sprintf("%s = convert(%d > %s)", variables[i[2]], i[0], variables[i[1]])
	},
	"gtri": func(i Parameters) string {
		return fmt.Sprintf("%s = convert(%s > %d)", variables[i[2]], variables[i[0]], i[1])
	},
	"gtrr": func(i Parameters) string {
		return fmt.Sprintf("%s = convert(%s > %s)", variables[i[2]], variables[i[0]], variables[i[1]])
	},

	"eqir": func(i Parameters) string {
		return fmt.Sprintf("%s = convert(%d == %s)", variables[i[2]], i[0], variables[i[1]])
	},
	"eqri": func(i Parameters) string {
		return fmt.Sprintf("%s = convert(%s == %d)", variables[i[2]], variables[i[0]], i[1])
	},
	"eqrr": func(i Parameters) string {
		return fmt.Sprintf("%s = convert(%s == %s)", variables[i[2]], variables[i[0]], variables[i[1]])
	},
}

func read() Program {
	var program Program
	fmt.Scanf("#ip %d\n", &program.ip)
	for {
		var instruction Instruction
		if _, err := fmt.Scanf("%s %d %d %d", &instruction.opCode, &instruction.params[0], &instruction.params[1], &instruction.params[2]); err != nil {
			break
		}
		program.instructions = append(program.instructions, instruction)
	}
	program.registers[0] = 0

	return program
}

var variables = []string{"a", "b", "c", "d", "e", "f"}

func isBranch(s string) bool {
	return strings.HasPrefix(s, "eq") || strings.HasPrefix(s, "gt") || s == ""
}

// Beware, you may find dragons here!
func translate(program Program) {
	fmt.Printf("func convert(b bool) int {\n\tif b { return 1 }\n\treturn 0\n}\n")
	for _, v := range variables {
		fmt.Printf("%s := 0\n", v)
		fmt.Printf("%s = %s\n", v, v)
	}
	lastOperation := make(map[string]string)
	usedLabels := make(map[string]bool)
	instructions := make([]string, 0)

	for i, instruction := range program.instructions {
		instructions = append(instructions, fmt.Sprintf("p%d:\n", i))
		if instruction.params[2] == program.ip {
			// This only supports several possible situations
			if instruction.opCode == "seti" {
				// Absolute jump
				label := fmt.Sprintf("p%d", instruction.params[0]+1)
				instructions = append(instructions, fmt.Sprintf("goto %s\n", label))
				usedLabels[label] = true
				continue
			}
			if instruction.opCode == "addi" && instruction.params[0] == program.ip {
				// Relative jump
				label := fmt.Sprintf("p%d", i+instruction.params[1]+1)
				instructions = append(instructions, fmt.Sprintf("goto %s\n", label))
				usedLabels[label] = true
				continue
			}
			if instruction.opCode == "addr" {
				if instruction.params[0] == program.ip && instruction.params[1] != program.ip {
					otherV := variables[instruction.params[1]]
					if isBranch(lastOperation[otherV]) {
						label := fmt.Sprintf("p%d", i+2)
						instructions = append(instructions, fmt.Sprintf("if %s == 1 { goto %s }\n", otherV, label))
						usedLabels[label] = true
						continue
					}
				}
				if instruction.params[1] == program.ip && instruction.params[0] != program.ip {
					otherV := variables[instruction.params[0]]
					if isBranch(lastOperation[otherV]) {
						label := fmt.Sprintf("p%d", i+2)
						instructions = append(instructions, fmt.Sprintf("if %s == 1 { goto %s }\n", otherV, label))
						usedLabels[label] = true
						continue
					}
				}
			}
			// Fallback to a whole bunch of if statements

			instructions = append(instructions, fmt.Sprintf("%s = %d\n", variables[program.ip], i))
			instructions = append(instructions, fmt.Sprintln(interpreter[instruction.opCode](instruction.params)))

			for j := range program.instructions {
				label := fmt.Sprintf("p%d", j+1)
				instructions = append(instructions, fmt.Sprintf("if %s == %d { goto %s }\n", variables[program.ip], j, label))
				usedLabels[label] = true
			}
			label := fmt.Sprintf("p%d", len(program.instructions))
			usedLabels[label] = true
			instructions = append(instructions, fmt.Sprintf("if %s >= %d { goto %s }\n", variables[program.ip], len(program.instructions), label))
			continue
		}

		if instruction.params[0] == program.ip || instruction.params[1] == program.ip {
			instructions = append(instructions, fmt.Sprintf("%s = %d\n", variables[program.ip], i))
		}
		lastOperation[variables[instruction.params[2]]] = instruction.opCode
		instructions = append(instructions, fmt.Sprintln(interpreter[instruction.opCode](instruction.params)))
	}
	instructions = append(instructions, fmt.Sprintf("p%d:\n", len(program.instructions)))

	for _, line := range instructions {
		if match, _ := regexp.Match("^p\\d+:\n$", []byte(line)); match && !usedLabels[line[:len(line)-2]] {
			continue
		}

		fmt.Print(line)
	}
}

func main() {
	program := read()
	translate(program)
}
