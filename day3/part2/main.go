package main

import (
	"fmt"
)

type rect struct {
	x, y, w, h int
}

func max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

func (this *rect) intersects(that rect) bool {
	right := this.x + this.w
	bottom := this.y + this.h

	thatRight := that.x + that.w
	thatBottom := that.y + that.h

	intersectsY := bottom > that.y && this.y <= that.y ||
		thatBottom > this.y && that.y <= this.y

	intersectsX := right > that.x && this.x <= that.x ||
		thatRight > this.x && that.x <= this.x

	return intersectsX && intersectsY
}

func (this *rect) extend(that rect) {
	right := this.x + this.w
	bottom := this.y + this.h
	if that.x < this.x {
		this.x = that.x
	}
	if that.y < this.y {
		this.y = that.y
	}
	newRight := max(right, that.x+that.w)
	newBottom := max(bottom, that.y+that.h)
	this.w = newRight - this.x
	this.h = newBottom - this.y
}

func (this *rect) area() int {
	return this.w * this.h
}

type claimrect struct {
	claim int
	rect
}

func readRects() ([]rect, []int) {
	rects := make([]rect, 0)
	claims := make([]int, 0)

	for {
		var r rect
		var claim int
		if _, err := fmt.Scanf("#%d @ %d,%d: %dx%d", &claim, &r.x, &r.y, &r.w, &r.h); err != nil {
			break
		}
		rects = append(rects, r)
		claims = append(claims, claim)
	}
	return rects, claims
}

func boundingBox(rects []rect) rect {
	bounds := rects[0]
	for _, el := range rects {
		bounds.extend(el)
	}
	return bounds
}

// This solution is super inefficient
func main() {
	rects, claims := readRects()

	for id, r := range rects {

		intersects := false

		for i := 0; i < len(rects); i++ {
			if i == id {
				continue
			}

			r2 := rects[i]
			if r.intersects(r2) {
				intersects = true
				break
			}
		}

		if !intersects {
			fmt.Println(claims[id])
		}
	}
}
