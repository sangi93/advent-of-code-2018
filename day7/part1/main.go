package main

import (
	"container/heap"
	"fmt"
)

type Node struct {
	indegree int
	label    string
	outEdges []string
}

type Edge struct {
	start, end string
}

func read() []Edge {
	results := make([]Edge, 0)

	for {
		var edge Edge
		if _, err := fmt.Scanf("Step %s must be finished before step %s can begin.\n", &edge.start, &edge.end); err != nil {
			break
		}

		results = append(results, edge)
	}

	return results
}

func generateGraph(edges []Edge) []*Node {
	nodes := make(map[string]*Node)

	for _, edge := range edges {
		if nodes[edge.start] == nil {
			nodes[edge.start] = &Node{label: edge.start}
		}
		if nodes[edge.end] == nil {
			nodes[edge.end] = &Node{label: edge.end}
		}

		nodes[edge.start].outEdges = append(nodes[edge.start].outEdges, edge.end)
		nodes[edge.end].indegree++
	}

	result := make([]*Node, 0)
	for _, node := range nodes {
		result = append(result, node)
	}

	return result
}

type PQNode struct {
	index int
	Node
	outNodes []*PQNode
}

type PriorityQueue []*PQNode

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	if pq[i].indegree == pq[j].indegree {
		return pq[i].label < pq[j].label
	}

	return pq[i].indegree < pq[j].indegree
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *PriorityQueue) Push(x interface{}) {
	n := len(*pq)
	item := x.(*PQNode)
	item.index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	item.index = -1 // for safety
	*pq = old[0 : n-1]
	return item
}

func (pq *PriorityQueue) update(item *PQNode) {
	heap.Fix(pq, item.index)
}

func findOrder(nodes []*Node) []*Node {

	pq := make(PriorityQueue, len(nodes))

	nodeMap := make(map[string]*PQNode)
	for id, node := range nodes {
		pq[id] = &PQNode{
			index: id,
			Node:  *node,
		}
		nodeMap[node.label] = pq[id]
	}
	for id, node := range nodes {
		pqnode := pq[id]
		for _, neighbour := range node.outEdges {
			pqnode.outNodes = append(pqnode.outNodes, nodeMap[neighbour])
		}
	}

	heap.Init(&pq)

	result := make([]*Node, 0)
	for pq.Len() > 0 {
		element := heap.Pop(&pq).(*PQNode)
		result = append(result, &element.Node)

		for _, neighbour := range element.outNodes {
			neighbour.indegree--
			pq.update(neighbour)
		}
	}
	return result
}

func main() {
	edges := read()

	nodes := generateGraph(edges)

	order := findOrder(nodes)
	for _, value := range order {
		fmt.Print(value.label)
	}
}
