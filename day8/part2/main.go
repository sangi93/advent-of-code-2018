package main

import "fmt"

func read() []int {
	result := make([]int, 0)

	for {
		var value int
		if _, err := fmt.Scanf("%d", &value); err != nil {
			break
		}

		result = append(result, value)
	}

	return result
}

// Outputs end pos and count
func metadataCount(values []int) (int, int) {
	children := values[0]
	metadata := values[1]

	counts := make(map[int]int)
	currentIndex := 2
	for i := 0; i < children; i++ {
		recPos, recCount := metadataCount(values[currentIndex:])
		currentIndex += recPos
		counts[i] = recCount
	}

	result := 0
	metadataSum := 0

	for i := 0; i < metadata; i++ {
		child := values[currentIndex]
		result += counts[child-1]
		metadataSum += child
		currentIndex++
	}

	if children == 0 {
		return currentIndex, metadataSum
	} else {
		return currentIndex, result
	}
}

func main() {
	values := read()

	_, count := metadataCount(values)

	fmt.Println(count)
}
