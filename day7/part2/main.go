package main

import (
	"fmt"
	"sort"
)

type Node struct {
	indegree int
	label    string
	outEdges []*Node
}

type Edge struct {
	start, end string
}

func read() []Edge {
	results := make([]Edge, 0)

	for {
		var edge Edge
		if _, err := fmt.Scanf("Step %s must be finished before step %s can begin.\n", &edge.start, &edge.end); err != nil {
			break
		}

		results = append(results, edge)
	}

	return results
}

func generateGraph(edges []Edge) []*Node {
	nodes := make(map[string]*Node)

	for _, edge := range edges {
		if nodes[edge.start] == nil {
			nodes[edge.start] = &Node{label: edge.start}
		}
		if nodes[edge.end] == nil {
			nodes[edge.end] = &Node{label: edge.end}
		}

		nodes[edge.start].outEdges = append(nodes[edge.start].outEdges, nodes[edge.end])
		nodes[edge.end].indegree++
	}

	result := make([]*Node, 0)
	for _, node := range nodes {
		result = append(result, node)
	}

	return result
}

type WorkerNode struct {
	timeLeft int
	*Node
}

func simulate(nodes []*Node, numWorkers int) int {
	workers := make(map[*WorkerNode]bool)
	allNodes := make([]*WorkerNode, len(nodes))
	for id, node := range nodes {
		allNodes[id] = &WorkerNode{
			timeLeft: 61 + int(node.label[0]) - int('A'),
			Node:     node,
		}
	}
	sort.Slice(allNodes, func(i, j int) bool {
		return allNodes[i].label < allNodes[j].label
	})

	time := 0
	for ; ; time++ {

		// Assign
		workLeft := false
		for _, node := range allNodes {
			if node.timeLeft > 0 || len(workers) > 0 {
				workLeft = true
			}
			if len(workers) == numWorkers {
				break
			}

			if node.indegree == 0 && node.timeLeft > 0 && len(workers) < numWorkers {
				workers[node] = true
			}
		}

		if !workLeft {
			break
		}

		// Tick
		for work, _ := range workers {
			work.timeLeft--

			// Distribute
			if work.timeLeft == 0 {
				for _, neighbour := range work.outEdges {
					neighbour.indegree--
				}
				delete(workers, work)
			}

		}

		//fmt.Println()
	}

	return time
}

func main() {
	edges := read()

	nodes := generateGraph(edges)

	time := simulate(nodes, 5)
	fmt.Println(time)

}
