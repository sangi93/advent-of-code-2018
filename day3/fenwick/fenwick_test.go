package fenwick

import (
	"testing"
)

func TestSet(t *testing.T) {
	fenwick := NewFenwick(5)
	fenwick.Update(3, 2)

	resultTable := []struct {
		position int
		value    int
	}{
		{0, 0},
		{2, 0},
		{3, 2},
		{4, 2},
	}

	for _, test := range resultTable {
		result := fenwick.Get(test.position)
		if result != test.value {
			t.Errorf("At position %d, expected sum %d, got sum %d", test.position, test.value, result)
		}
	}
}

func TestRange(t *testing.T) {
	fenwick := NewFenwick(5)
	fenwick.UpdateRange(1, 4, 1)

	resultTable := []struct {
		position int
		value    int
	}{
		{0, 0},
		{1, 1},
		{3, 1},
		{4, 0},
	}

	for _, test := range resultTable {
		result := fenwick.Get(test.position)
		if result != test.value {
			t.Errorf("At position %d, expected sum %d, got sum %d", test.position, test.value, result)
		}
	}
}

func TestRangeCombined(t *testing.T) {
	fenwick := NewFenwick(6)
	fenwick.UpdateRange(1, 3, 1)
	fenwick.UpdateRange(2, 4, 1)

	resultTable := []struct {
		position int
		value    int
	}{
		{0, 0},
		{1, 1},
		{2, 2},
		{3, 1},
		{4, 0},
	}

	for _, test := range resultTable {
		result := fenwick.Get(test.position)
		if result != test.value {
			t.Errorf("At position %d, expected sum %d, got sum %d", test.position, test.value, result)
		}
	}
}

func TestSet2D(t *testing.T) {
	fenwick := NewFenwick2D(5, 6)
	fenwick.Update(3, 2, 4)

	resultTable := []struct {
		x     int
		y     int
		value int
	}{
		{0, 0, 0},
		{2, 5, 0},
		{3, 2, 4},
		{4, 4, 4},
		{4, 1, 0},
	}

	for _, test := range resultTable {
		result := fenwick.Get(test.x, test.y)
		if result != test.value {
			t.Errorf("At position (%d, %d), expected sum %d, got sum %d", test.x, test.y, test.value, result)
		}
	}
}

func TestSet2DRange(t *testing.T) {
	fenwick := NewFenwick2D(5, 6)
	fenwick.UpdateRange(2, 1, 3, 5, 3)

	resultTable := []struct {
		x     int
		y     int
		value int
	}{
		{0, 0, 0},
		{2, 1, 3},
		{2, 4, 3},
		{2, 5, 0},
		{3, 4, 0},
		{3, 5, 0},
	}

	for _, test := range resultTable {
		result := fenwick.Get(test.x, test.y)
		if result != test.value {
			t.Errorf("At position (%d, %d), expected sum %d, got sum %d", test.x, test.y, test.value, result)
		}
	}
}

func TestSet2DRangeCombined(t *testing.T) {
	fenwick := NewFenwick2D(5, 6)
	fenwick.UpdateRange(2, 1, 3, 5, 3)
	fenwick.UpdateRange(2, 2, 4, 3, 1)

	resultTable := []struct {
		x     int
		y     int
		value int
	}{
		{0, 0, 0},
		{2, 1, 3},
		{2, 2, 4},
		{2, 4, 3},
		{2, 5, 0},
		{3, 2, 1},
		{3, 5, 0},
	}

	for _, test := range resultTable {
		result := fenwick.Get(test.x, test.y)
		if result != test.value {
			t.Errorf("At position (%d, %d), expected sum %d, got sum %d", test.x, test.y, test.value, result)
		}
	}
}
