import sys

def intersect(w1, w2):
    str = ""
    for a, b in zip(w1, w2):
        if a == b:
            str += a

    return str

values = []

for line in sys.stdin:
    for v in values:
        excluded = intersect(line, v)
        if len(excluded) == len(line) - 1:
            print(excluded)
            exit()

    values.append(line)
